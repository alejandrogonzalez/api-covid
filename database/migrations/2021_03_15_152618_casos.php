<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Casos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casos',function (Blueprint $table){
           $table->id();
           $table->date('fecha');
           $table->integer('ccaas_id');
           $table->integer('numero');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('casos',function (Blueprint $table){
           $table->dropColumn('fecha');
           $table->dropColumn('ccaas_id');
           $table->dropColumn('numero');
           $table->foreign('pais_id')->references('id')->on('ccaas')->onDelete('cascade');
        });
    }
}
