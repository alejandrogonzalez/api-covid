<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Ia14Controller;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('ia14',Ia14Controller::class);
Route::get('ia14/{fecha}',[Ia14Controller::class,'show']);
Route::get('ia14all',[Ia14Controller::class,'showAll']);
Route::post('ia14add',[Ia14Controller::class,'store']);

Route::get('/ia14/{fecha}/{fecha2}',[Ia14Controller::class,'showCollection']);

Route::resource('ia7',Ia7Controller::class);
Route::get('ia/{fecha}',[Ia7Controller::class,'show']);
Route::get('ia7all',[ia7Controller::class,'showAll']);
Route::post('ia7add',[Ia7Controller::class,'store']);

Route::resource('paises',PaisesController::class);
Route::get('paises/{fecha}',[PaisesController::class,'show']);
Route::get('paisesAll',[paisesController::class,'showAll']);
//Route::post('paisesAdd'[PaisesController::class,'store']);



